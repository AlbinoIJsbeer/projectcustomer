﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwitcher : MonoBehaviour
{
    [SerializeField]
    private GameObject _redPlayer;
    [SerializeField]
    private GameObject _bluePlayer;
    [SerializeField]
    private GameObject _greenPlayer;

    private void Start()
    {
        _redPlayer.SetActive(true);
        _bluePlayer.SetActive(false);
        _greenPlayer.SetActive(false);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            _redPlayer.SetActive(true);
            _bluePlayer.SetActive(false);
            _greenPlayer.SetActive(false);
        }else if (Input.GetKeyDown(KeyCode.K))
        {
            _redPlayer.SetActive(false);
            _bluePlayer.SetActive(true);
            _greenPlayer.SetActive(false);
        }else if (Input.GetKeyDown(KeyCode.L))
        {
            _redPlayer.SetActive(false);
            _bluePlayer.SetActive(false);
            _greenPlayer.SetActive(true);
        }
    }
}
