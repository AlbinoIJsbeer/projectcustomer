﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TrashCollector : MonoBehaviour
{
    [SerializeField]
    private GameObject _player;

    [SerializeField]
    private int _redScore = 0;
    [SerializeField]
    private int _blueScore = 0;
    [SerializeField]

    private int _greenScore = 0;    
    
    [SerializeField]
    private int _redCollectedScore = 0;
    [SerializeField]
    private int _blueCollectedScore = 0;
    [SerializeField]
    private int _greenCollectedScore = 0;

    [SerializeField]
    private GameObject _prefab;

    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private float _range = 30f;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Interact();
        }


        if (_redCollectedScore >= 5)
        {
            _prefab.SetActive(true);
            _redCollectedScore = 0;
        }

    }


    private void Interact()
    {
        RaycastHit _hit;
        if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out _hit, _range))
        {
            Debug.Log("Hit: " + _hit.transform.tag);
            if (_hit.transform.tag == "Red Trash")
            {
                _redScore += 1;
                Destroy(_hit.transform.gameObject);
            }
            if (_hit.transform.tag == "Blue Trash")
            {
                _blueScore += 1;
                Destroy(_hit.transform.gameObject);
            }

            if (_hit.transform.tag == "Green Trash")
            {
                _greenScore += 1;
                Destroy(_hit.transform.gameObject);
            }

            if (_hit.transform.tag == "Red Bin")
            {
                _redCollectedScore = _redScore;
                _redScore = 0;
            }
            if (_hit.transform.tag == "Blue Bin")
            {
                _blueCollectedScore = _blueScore;
                _blueScore = 0;
            }
            if (_hit.transform.tag == "Green Bin")
            {
                _greenCollectedScore = _greenScore;
                _greenScore = 0;
            }
        }
    }

/*  
    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Entered: " + collision.gameObject.tag);

        if (collision.gameObject.tag == "Red Trash")
        {
            _redScore += 1;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "Blue Trash")
        {
            _blueScore += 1;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Green Trash")
        {
            _greenScore += 1;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Red Bin")
        {
            _redCollectedScore = _redScore;
            _redScore = 0;
        }
        if (collision.gameObject.tag == "Blue Bin")
        {
            _blueCollectedScore = _blueScore;
            _blueScore = 0;
        }
        if (collision.gameObject.tag == "Green Bin")
        {
            _greenCollectedScore = _greenScore;
            _greenScore = 0;
        }
    }
*/

}
