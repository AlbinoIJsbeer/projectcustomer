﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAround : MonoBehaviour
{

    public float MouseSensitivity = 100f;

    [SerializeField]
    private Transform _playerBody;

    private float _xRotation = 0f;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }


    private void Update()
    {
        float _mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
        float _mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

        _xRotation -= _mouseY;
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);


        transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);

        _playerBody.Rotate(Vector3.up * _mouseX);
    }
}
