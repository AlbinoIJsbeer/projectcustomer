﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private CharacterController _controller;
    private float _speed = 12.0f;
    private float _gravity = -9.81f * 0.05f;

    private Vector3 _velocity;
    private float _jumpHeight = 0.05f;

    [SerializeField]
    private Transform _checkForGround;
    [SerializeField]
    private float _groundDistance = 0.4f;
    [SerializeField]
    private LayerMask _groundMask;

    [SerializeField]
    private bool _isGrounded;

    void Update()
    {

        _isGrounded = Physics.CheckSphere(_checkForGround.position, _groundDistance, _groundMask);

        if (_isGrounded && _velocity.y < 0)
        {
            _velocity.y = 0f;
        }

        float _x = Input.GetAxis("Horizontal");
        float _z = Input.GetAxis("Vertical");

        Vector3 _move = this.transform.right * _x + this.transform.forward * _z;

        _controller.Move(_move * _speed * Time.deltaTime);

        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _velocity.y = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
        }


        _velocity.y += _gravity * Time.deltaTime;
        _controller.Move(_velocity);
    }

}
